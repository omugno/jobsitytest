package com.jobsity.test.series.core.models

data class Episode(
    val id: Int,
    val name: String?,
    val number: Int?,
    val type: String?,
    val image: Banner?,
    val season: Int?,
    val summary: String?
)
