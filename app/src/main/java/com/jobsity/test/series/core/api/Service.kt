package com.jobsity.test.series.core.api

import com.jobsity.test.series.core.models.Episode
import com.jobsity.test.series.core.models.SearchShowResult
import com.jobsity.test.series.core.models.Season
import com.jobsity.test.series.core.models.Show
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface Service {

    @GET("shows")
    suspend fun list(@Query("page") page: Int): MutableList<Show>

    @GET("search/shows")
    suspend fun search(@Query("q") query: String): MutableList<SearchShowResult>

    @GET("shows/{id}")
    suspend fun get(@Path("id") id: Int): Show

    @GET("seasons/{id}/episodes")
    suspend fun episodes(@Path("id") id: Int): MutableList<Episode>

    @GET("episodes/{id}")
    suspend fun episode(@Path("id") id: Int): Episode

    @GET("shows/{id}/seasons")
    suspend fun seasons(@Path("id") id: Int): MutableList<Season>
}