package com.jobsity.test.series.ui.shows

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jobsity.test.series.core.api.Repository
import com.jobsity.test.series.core.models.Show
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ShowsViewModel : ViewModel() {
    private var page: Int = 1
    val loading: MutableLiveData<Boolean> = MutableLiveData()
    val shows: MutableLiveData<MutableList<Show>> = MutableLiveData()

    fun init() {
        loading.postValue(true)
        viewModelScope.launch(Dispatchers.IO) {
            Repository.list(page)?.let {
                shows.postValue(it)
            }
            loading.postValue(false)
        }
    }

    fun loadMore() {
        page++
        loading.postValue(true)
        viewModelScope.launch(Dispatchers.IO) {
            val showsData = shows.value ?: mutableListOf()
            Repository.list(page)?.let {
                showsData.addAll(it)
                shows.postValue(showsData)
            }
            loading.postValue(false)
        }
    }
}