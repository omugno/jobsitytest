package com.jobsity.test.series.ui.show.episodes

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jobsity.test.series.R
import com.jobsity.test.series.core.models.Episode
import com.jobsity.test.series.databinding.EpisodesFragmentBinding
import com.jobsity.test.series.ui.episode.adapters.EpisodeRecyclerViewAdapter
import com.jobsity.test.series.ui.show.ShowFragmentDirections

class EpisodesFragment : Fragment() {

    companion object {
        val ARGUMENT_ID: String = "id"

        fun newInstance(id: Int): EpisodesFragment {
            var f = EpisodesFragment()
            val bundle = Bundle()
            bundle.putInt(ARGUMENT_ID, id)
            f.setArguments(bundle)
            return f
        }

        @JvmStatic
        @BindingAdapter("data")
        fun setRecyclerViewProperties(recyclerView: RecyclerView?, data: MutableList<Episode>?) {
            val adapter = recyclerView?.adapter
            if (adapter is EpisodeRecyclerViewAdapter && data != null) {
                adapter.setEpisodes(data)
            }
        }
    }

    private val viewModel: EpisodesViewModel by viewModels()
    private lateinit var binding: EpisodesFragmentBinding
    private var seasonNumber: Int = -1

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.episodes_fragment, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.let {
            viewModel.load(it.getInt(ARGUMENT_ID))
        }
        setupView()
    }

    private fun setupView() {
        binding.episodesList.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = EpisodeRecyclerViewAdapter(mutableListOf()) {
                findNavController().navigate(
                    ShowFragmentDirections.actionShowFragmentToEpisodeFragment(it.id)
                )
            }
        }
    }
}