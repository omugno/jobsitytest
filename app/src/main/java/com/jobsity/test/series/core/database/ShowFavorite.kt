package com.jobsity.test.series.core.database

import com.jobsity.test.series.core.Constants
import com.jobsity.test.series.core.models.Show
import io.realm.Realm
import io.realm.RealmConfiguration

object ShowFavorite {
    val mReal by lazy { createThreadRealm() }

    /**
     * save new favorite on database
     */
    fun add(show: Show) {
        var real = createThreadRealm()
        real.executeTransaction { transaction ->
            transaction.insert(show)
        }
        real.close()
    }

    /**
     * delete favorite on database
     */
    fun remove(show: Show) {
        var real = createThreadRealm()
        real.executeTransaction {
            val result = it.where(Show::class.java).equalTo("id", show.id).findFirst()
            result?.deleteFromRealm()
        }
        real.close()
    }

    /**
     * list of favorite
     */
    fun list(): MutableList<Show> {
        var shows: MutableList<Show> = mutableListOf()
        mReal.executeTransaction {
            shows = it.where(Show::class.java).sort("name").findAll().toMutableList()
        }
        return shows
    }


    /**
     * check if it's a favorite show
     */
    fun check(id: Int): Boolean {
        var real = createThreadRealm()
        var show: Show? = null
        real.executeTransaction {
            show = it.where(Show::class.java).equalTo("id", id).findFirst()
        }
        real.close()
        return show != null
    }

    private fun createThreadRealm(): Realm = Realm.getInstance(
        RealmConfiguration.Builder().allowWritesOnUiThread(true).allowQueriesOnUiThread(true)
            .name(Constants.DATA_BASE_NAME).build()
    )
}