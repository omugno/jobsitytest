package com.jobsity.test.series.ui.favorites

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jobsity.test.series.core.database.ShowFavorite
import com.jobsity.test.series.core.models.Show

class FavoritesViewModel : ViewModel() {
    val loading: MutableLiveData<Boolean> = MutableLiveData()
    val shows: MutableLiveData<MutableList<Show>> = MutableLiveData()

    fun init() {
        loading.postValue(true)
        val list: MutableList<Show>? = ShowFavorite.list()
        println(list)
        shows.postValue(list)
        loading.postValue(false)
    }
}