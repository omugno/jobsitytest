package com.jobsity.test.series.core.models

import io.realm.RealmList
import io.realm.RealmObject

open class Show(
    var id: Int? = null,
    var name: String? = null,
    var type: String? = null,
    var image: Banner? = null,
    var language: String? = null,
    var summary: String? = null,
    var genres: RealmList<String>? = null,
    var schedule: Schedule? = null
) : RealmObject()

data class SearchShowResult(
    val score: Double?,
    val show: Show
)

open class Schedule(
    var time: String? = null,
    var days: RealmList<String>? = null
) : RealmObject()