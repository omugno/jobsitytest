package com.jobsity.test.series.ui.episode

import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.jobsity.test.series.R
import com.jobsity.test.series.core.models.Episode
import com.jobsity.test.series.databinding.EpisodeFragmentBinding
import com.squareup.picasso.Picasso

class EpisodeFragment : Fragment() {
    private val args: EpisodeFragmentArgs by navArgs()
    private val viewModel: EpisodeViewModel by viewModels()
    private lateinit var binding: EpisodeFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.episode_fragment, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.load(args.episodeId)
        viewModel.episode.observe(viewLifecycleOwner) {
            it?.let {
                fillData(it)
            }
        }
    }

    private fun fillData(episode: Episode) {
        binding.name.text = "${episode.number} ${episode.name}"
        binding.season.text = getString(R.string.season, episode.season)
        binding.summary.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(episode.summary, Html.FROM_HTML_MODE_COMPACT)
        } else {
            Html.fromHtml(episode.summary)
        }
        episode.image?.let {
            Picasso.get().load(it.medium).into(binding.image)
        }
    }
}