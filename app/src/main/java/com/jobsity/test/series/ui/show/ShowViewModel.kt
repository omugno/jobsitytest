package com.jobsity.test.series.ui.show

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jobsity.test.series.core.api.Repository
import com.jobsity.test.series.core.database.ShowFavorite
import com.jobsity.test.series.core.models.Season
import com.jobsity.test.series.core.models.Show
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ShowViewModel : ViewModel() {
    val loading: MutableLiveData<Boolean> = MutableLiveData()
    val show: MutableLiveData<Show> = MutableLiveData()
    val seasons: MutableLiveData<MutableList<Season>> = MutableLiveData()
    val isFavorite: MutableLiveData<Boolean> = MutableLiveData()

    fun load(id: Int) {
        loading.postValue(true)
        viewModelScope.launch(Dispatchers.IO) {
            Repository.get(id)?.let {
                show.postValue(it)
                isFavorite.postValue(ShowFavorite.check(id))
            }
            Repository.seasons(id)?.let {
                seasons.postValue(it)
            }
            loading.postValue(false)
        }
    }

    fun removeFavorite() {
        show.value?.let {
            ShowFavorite.remove(it)
            isFavorite.postValue(false)
        }
    }

    fun addFavorite() {
        show.value?.let {
            ShowFavorite.add(it)
            isFavorite.postValue(true)
        }
    }
}