package com.jobsity.test.series.core.models

import io.realm.RealmObject

open class Banner(
    var medium: String? = null,
    var original: String? = null
) : RealmObject()