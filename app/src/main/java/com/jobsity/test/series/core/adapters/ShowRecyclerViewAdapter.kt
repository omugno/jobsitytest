package com.jobsity.test.series.core.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.jobsity.test.series.R
import com.jobsity.test.series.core.database.ShowFavorite
import com.jobsity.test.series.core.models.Show
import com.squareup.picasso.Picasso

/**
 * Recycler to paint show list
 */
class ShowRecyclerViewAdapter(
    private val shows: MutableList<Show>,
    private val itemClick: (show: Show) -> Unit
) : RecyclerView.Adapter<ShowRecyclerViewAdapter.ShowViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShowViewHolder {
        return ShowViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.show_item_view,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = shows.size

    override fun onBindViewHolder(holder: ShowViewHolder, position: Int) {
        holder.bind(shows[position])
        holder.itemView.setOnClickListener {
            var show: Show = shows[holder.adapterPosition]
            itemClick(show)
        }
    }

    fun setShows(shows: MutableList<Show>) {
        this.shows.clear()
        this.shows.addAll(shows)
        notifyDataSetChanged()
    }

    inner class ShowViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bind(show: Show) {
            itemView.findViewById<TextView>(R.id.name).text = show.name
            show.genres?.let {
                itemView.findViewById<TextView>(R.id.genres).text = it.joinToString()
            }
            show.image?.let {
                Picasso.get().load(it.medium).into(itemView.findViewById<ImageView>(R.id.image))
            }
            val favoriteBtn = itemView.findViewById<ImageButton>(R.id.favorite)
            var isFavorite = false
            show.id?.let {
                isFavorite = ShowFavorite.check(it)
            }
            updateFavorite(show, isFavorite)
        }

        private fun updateFavorite(show: Show, isFavorite: Boolean) {
            val favoriteBtn = itemView.findViewById<ImageButton>(R.id.favorite)
            if (isFavorite) {
                favoriteBtn.setImageResource(R.drawable.ic_star)
                favoriteBtn.setOnClickListener {
                    ShowFavorite.remove(show)
                    updateFavorite(show, false)
                }
            } else {
                favoriteBtn.setImageResource(R.drawable.ic_star_border)
                favoriteBtn.setOnClickListener {
                    ShowFavorite.add(show)
                    updateFavorite(show, true)
                }
            }
        }
    }
}