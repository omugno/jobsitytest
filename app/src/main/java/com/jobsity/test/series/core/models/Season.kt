package com.jobsity.test.series.core.models

data class Season(
    val id: Int,
    val name: String?,
    val number: Int?
)
