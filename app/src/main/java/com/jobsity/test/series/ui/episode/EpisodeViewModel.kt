package com.jobsity.test.series.ui.episode

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jobsity.test.series.core.api.Repository
import com.jobsity.test.series.core.models.Episode
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class EpisodeViewModel : ViewModel() {
    val loading: MutableLiveData<Boolean> = MutableLiveData()
    val episode: MutableLiveData<Episode> = MutableLiveData()

    fun load(id: Int) {
        loading.postValue(true)
        viewModelScope.launch(Dispatchers.IO) {
            Repository.episode(id)?.let {
                episode.postValue(it)
            }
            loading.postValue(false)
        }
    }
}