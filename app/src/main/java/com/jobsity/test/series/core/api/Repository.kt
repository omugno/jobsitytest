package com.jobsity.test.series.core.api

import com.jobsity.test.series.BuildConfig
import com.jobsity.test.series.core.models.Episode
import com.jobsity.test.series.core.models.SearchShowResult
import com.jobsity.test.series.core.models.Season
import com.jobsity.test.series.core.models.Show
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object Repository {
    val retrofit: Retrofit by lazy { provideRetrofit() }

    // api retrofit object to request data to api
    val api by lazy { retrofit.create(Service::class.java) }

    /**
     * List shows method for get show by pagination
     * @param page number of page to list
     * @return list of shows
     */
    suspend fun list(page: Int): MutableList<Show>? = execute { api.list(page) }

    /**
     * List shows method that match with query text
     * @param query string with text to search show's that match with it
     * @return List of show that match with the query
     */
    suspend fun search(query: String): MutableList<SearchShowResult>? =
        execute { api.search(query) }

    /**
     * Get detail of show by id
     * @param id the id of the show that it's want to get complete info
     * @return Show at that id
     */
    suspend fun get(id: Int): Show? = execute { api.get(id) }

    /**
     * List of seasons of a show
     * @param id the id of the show that it's want to get seasons
     * @return List of seasons
     */
    suspend fun seasons(id: Int): MutableList<Season>? = execute { api.seasons(id) }

    /**
     * List of episodes by season id
     * @param id the id of the season that it's want to get episodes
     * @return List of episodes
     */
    suspend fun episodes(id: Int): MutableList<Episode>? = execute { api.episodes(id) }

    /**
     * Get episode by id
     * @param id the id of the episode that it's want to get details
     * @return Episode of that id
     */
    suspend fun episode(id: Int): Episode? = execute { api.episode(id) }

    private fun provideRetrofit(): Retrofit = Retrofit.Builder()
        .baseUrl(BuildConfig.SERVER_URL)
        .addConverterFactory(GsonConverterFactory.create()).build()

    /**
     * High order function to manage exceptions
     */
    private suspend fun <T> execute(block: suspend () -> T): T? {
        try {
            return block()
        } catch (e: Exception) {

        }
        return null
    }
}