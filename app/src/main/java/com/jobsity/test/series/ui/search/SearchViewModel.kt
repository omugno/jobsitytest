package com.jobsity.test.series.ui.search

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jobsity.test.series.core.api.Repository
import com.jobsity.test.series.core.models.Show
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SearchViewModel : ViewModel() {
    var query: MutableLiveData<CharSequence> = MutableLiveData()
    val loading: MutableLiveData<Boolean> = MutableLiveData()
    val shows: MutableLiveData<MutableList<Show>> = MutableLiveData()

    fun search(query: CharSequence) {
        loading.postValue(true)
        viewModelScope.launch(Dispatchers.IO) {
            Repository.search(query.toString())?.let {
                it.map { item -> item.show }.toMutableList().let { list -> shows.postValue(list) }
            }
            loading.postValue(false)
        }
    }
}