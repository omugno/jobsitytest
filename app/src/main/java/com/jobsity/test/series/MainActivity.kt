package com.jobsity.test.series

import android.content.Intent
import android.content.pm.PackageManager
import android.hardware.biometrics.BiometricManager.Authenticators.*
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import androidx.navigation.findNavController

class MainActivity : AppCompatActivity() {
    companion object {
        const val FINGERPRINT_REQUEST_CODE = 1;
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        setSupportActionBar(findViewById(R.id.toolbar))

        // call checkSecureMethod for authenticate user
        checkSecureMethod()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    /*
    * Method for handle select menu option
     */
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_search -> {
            val controller = findNavController(R.id.nav_host_fragment)
            if (controller.currentDestination?.id != R.id.searchFragment)
                controller.navigate(R.id.action_searchFragment)
            true
        }
        R.id.action_favorite -> {
            val controller = findNavController(R.id.nav_host_fragment)
            if (controller.currentDestination?.id != R.id.favoritesFragment)
                controller.navigate(R.id.action_favoritesFragment)
            true
        }
        R.id.action_home -> {
            val controller = findNavController(R.id.nav_host_fragment)
            if (controller.currentDestination?.id != R.id.showFragment)
                controller.navigate(R.id.action_showsFragment)
            true
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    /*
    * This method it's for request user authentication using available methods to secure device
     */
    private fun checkSecureMethod() {
        // check for version of android first then
        // check the right method for check secure method
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            checkSecureMethod(BIOMETRIC_STRONG or DEVICE_CREDENTIAL) {
                println("Error on access to secure method")
            }
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkSecureMethod(BIOMETRIC_WEAK) {
                println("Error on access to secure method")
            }
        }
    }

    /*
    * this method it's for check type of secure methods available
    * only for android version 11 or above
    * in case it's available but don't enrolled request for settings
    * in case it's success request method for show dialog
    * in case error call noSupport method
     */
    private fun checkSecureMethod(type: Int, noSupportCb: () -> Unit) {
        val biometricManager = BiometricManager.from(this)
        when (biometricManager.canAuthenticate(type)) {
            BiometricManager.BIOMETRIC_SUCCESS -> requestAuthentication(type)
            BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE -> noSupportCb()
            BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE -> noSupportCb()
            BiometricManager.BIOMETRIC_STATUS_UNKNOWN -> noSupportCb()
            BiometricManager.BIOMETRIC_ERROR_UNSUPPORTED -> noSupportCb()
            BiometricManager.BIOMETRIC_ERROR_SECURITY_UPDATE_REQUIRED -> noSupportCb()
            BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> {
                try {
                    val enrollIntent = Intent(Settings.ACTION_BIOMETRIC_ENROLL).apply {
                        putExtra(Settings.EXTRA_BIOMETRIC_AUTHENTICATORS_ALLOWED, type)
                    }
                    startActivityForResult(enrollIntent, FINGERPRINT_REQUEST_CODE)
                } catch (e: Exception) {
                    startActivity(Intent(Settings.ACTION_SETTINGS))
                }
            }
        }
    }

    /*
    * Method to request device Authentication
    * in case it's fail or cancel by user the App have to close
    * only it's succeeded will allow to manipulate the app
     */
    private fun requestAuthentication(type: Int) {
        BiometricPrompt(this, ContextCompat.getMainExecutor(this),
            object : BiometricPrompt.AuthenticationCallback() {
                override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                    super.onAuthenticationError(errorCode, errString)
                    finish()
                }

                override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                    super.onAuthenticationSucceeded(result)
                    Toast.makeText(
                        applicationContext,
                        "Authentication succeeded!", Toast.LENGTH_SHORT
                    )
                        .show()
                }

                override fun onAuthenticationFailed() {
                    super.onAuthenticationFailed()
                    finish()
                }
            }).authenticate(
            BiometricPrompt.PromptInfo.Builder()
                .setTitle(getString(R.string.dialog_secure_method_title))
                .setSubtitle(getString(R.string.dialog_secure_method_subtitle))
                .setNegativeButtonText(getString(R.string.dialog_secure_method_out))
                .setAllowedAuthenticators(type)
                .build()
        )
    }

    /*
    * Wait response for settings to configure authentication method
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == FINGERPRINT_REQUEST_CODE && resultCode != 0) {
            checkSecureMethod()
        }
    }

    /*
    * Wait response for accept or decline permission to access to Fingerprint
     */
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == FINGERPRINT_REQUEST_CODE && grantResults[0] == PackageManager.PERMISSION_GRANTED)
            checkSecureMethod()
    }
}