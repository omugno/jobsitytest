package com.jobsity.test.series.ui.show

import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.google.android.material.tabs.TabLayout.TabLayoutOnPageChangeListener
import com.jobsity.test.series.R
import com.jobsity.test.series.core.models.Season
import com.jobsity.test.series.core.models.Show
import com.jobsity.test.series.databinding.ShowFragmentBinding
import com.jobsity.test.series.ui.show.adapters.SeasonsAdapter
import com.squareup.picasso.Picasso


class ShowFragment : Fragment() {
    private val args: ShowFragmentArgs by navArgs()
    private val viewModel: ShowViewModel by viewModels()
    private lateinit var binding: ShowFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.show_fragment, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.load(args.showId)
        viewModel.show.observe(viewLifecycleOwner) {
            it?.let {
                fillData(it)
            }
        }
        viewModel.seasons.observe(viewLifecycleOwner) {
            it?.let {
                showTabs(it)
            }
        }
        viewModel.isFavorite.observe(viewLifecycleOwner) {
            if (it) {
                binding.favorite.setImageResource(R.drawable.ic_star)
            } else {
                binding.favorite.setImageResource(R.drawable.ic_star_border)
            }
        }
        binding.favorite.setOnClickListener {
            if (viewModel.isFavorite.value == true) {
                viewModel.removeFavorite()
            } else {
                viewModel.addFavorite()
            }
        }
    }

    private fun fillData(show: Show) {
        binding.name.text = show.name
        show.genres?.let {
            binding.genres.text = it.joinToString()
        }
        binding.schedule.text = "${show.schedule?.time}  -  ${show.schedule?.days?.joinToString()}"
        binding.summary.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(show.summary, Html.FROM_HTML_MODE_COMPACT)
        } else {
            Html.fromHtml(show.summary)
        }
        show.image?.let {
            Picasso.get().load(it.medium).into(binding.image)
        }
    }

    private fun showTabs(seasons: MutableList<Season>) {
        seasons.forEach {
            val title = if (it.name?.isNotBlank() == true) it.name else it.number.toString()
            binding.tabs.addTab(binding.tabs.newTab().setText(title))
        }
        val adapter = SeasonsAdapter(parentFragmentManager, seasons)
        binding.viewPager.adapter = adapter
        //binding.viewPager.setOffscreenPageLimit(1)
        binding.viewPager.addOnPageChangeListener(TabLayoutOnPageChangeListener(binding.tabs))
        binding.tabs.setupWithViewPager(binding.viewPager)
    }
}