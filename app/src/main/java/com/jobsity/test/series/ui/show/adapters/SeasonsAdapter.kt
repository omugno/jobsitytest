package com.jobsity.test.series.ui.show.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.jobsity.test.series.core.models.Season
import com.jobsity.test.series.ui.show.episodes.EpisodesFragment

class SeasonsAdapter(fm: FragmentManager, private var seasons: MutableList<Season>) :
    FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return EpisodesFragment.newInstance(seasons[position].id)
    }

    override fun getCount(): Int = seasons.size

    override fun getPageTitle(position: Int): CharSequence? {
        return if (seasons[position].name?.isNotBlank() == true) seasons[position].name else seasons[position].number.toString()
    }
}