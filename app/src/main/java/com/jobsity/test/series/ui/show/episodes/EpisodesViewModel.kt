package com.jobsity.test.series.ui.show.episodes

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jobsity.test.series.core.api.Repository
import com.jobsity.test.series.core.models.Episode
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class EpisodesViewModel : ViewModel() {
    val loading: MutableLiveData<Boolean> = MutableLiveData()
    val episodes: MutableLiveData<MutableList<Episode>> = MutableLiveData()

    fun load(id: Int) {
        loading.postValue(true)
        viewModelScope.launch(Dispatchers.IO) {
            Repository.episodes(id)?.let {
                episodes.postValue(it)
            }
            loading.postValue(false)
        }
    }
}