package com.jobsity.test.series.ui.episode.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.jobsity.test.series.R
import com.jobsity.test.series.core.models.Episode

class EpisodeRecyclerViewAdapter(
    private val episodes: MutableList<Episode>,
    private val itemClick: (episode: Episode) -> Unit
) :
    RecyclerView.Adapter<EpisodeRecyclerViewAdapter.EpisodeViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EpisodeViewHolder {
        return EpisodeViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.episode_item_view,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = episodes.size

    override fun onBindViewHolder(holder: EpisodeViewHolder, position: Int) {
        holder.bind(episodes[position])
        holder.itemView.setOnClickListener {
            var episode: Episode = episodes[holder.adapterPosition]
            itemClick(episode)
        }
    }

    fun setEpisodes(episodes: MutableList<Episode>) {
        this.episodes.clear()
        this.episodes.addAll(episodes)
        notifyDataSetChanged()
    }

    inner class EpisodeViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bind(episode: Episode) {
            itemView.findViewById<TextView>(R.id.text).text = "${episode.number}. ${episode.name}"
        }
    }
}