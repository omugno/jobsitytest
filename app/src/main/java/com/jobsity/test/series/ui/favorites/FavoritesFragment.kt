package com.jobsity.test.series.ui.favorites

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jobsity.test.series.R
import com.jobsity.test.series.core.adapters.FavoriteShowRecyclerViewAdapter
import com.jobsity.test.series.core.models.Show
import com.jobsity.test.series.databinding.FavoritesFragmentBinding

class FavoritesFragment : Fragment() {
    private val viewModel: FavoritesViewModel by viewModels()
    private lateinit var binding: FavoritesFragmentBinding

    companion object {
        @JvmStatic
        @BindingAdapter("items")
        fun setRecyclerViewProperties(recyclerView: RecyclerView?, data: MutableList<Show>?) {
            val adapter = recyclerView?.adapter
            if (adapter is FavoriteShowRecyclerViewAdapter && data != null) {
                adapter.setShows(data)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.favorites_fragment, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupView()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        viewModel.init()
    }

    private fun setupView() {
        binding.showList.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = FavoriteShowRecyclerViewAdapter(mutableListOf()) {
                it.id?.let { id ->
                    findNavController().navigate(
                        FavoritesFragmentDirections.actionFavoritesFragmentToShowFragment(
                            id
                        )
                    )
                }
            }
        }
    }
}